class Facility < ActiveRecord::Base
  # name  :string
  
  has_many :registers, :through => :facility_registers
end

class Register < ActiveRecord::Base
  # name          :string
  # specific      :boolean ("specific" if true or "shared" if false, determines if it can belong to one or many facilities)
  # register_type :integer
  
  has_many :facilities, :through => :facility_registers
  has_many :activities
end

class SpecialRegisterA < Register
  before_save :set_correct_register_type
  default_scope :conditions => { :register_type => 1}

protected
  def set_correct_register_type
    self.register_type = 1
  end
end

class SpecialRegisterB < Register
  before_save :set_correct_register_type
  default_scope :conditions => { :register_type => 2}

protected
  def set_correct_register_type
    self.register_type = 2
  end
end

class Activity < ActiveRecord::Base
  # name            :string 
  # legislations    :string 
  # under_review    :boolean
  # aspect_id       :integer
  # impact_id       :integer
  # impact_rating   :integer
  # register_id     :integer
  
  belongs_to :register
  belongs_to :aspect
  belongs_to :impact
end